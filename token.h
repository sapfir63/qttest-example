#ifndef TOKEN_H
#define TOKEN_H

#include <QtCore>

// Object representing a single entity in a mathematical expression - a number or an operator
class Token {
public:
    enum TokenType {
        IntegerToken,
        OperatorToken
    };

    // Default constructor for usage in QList
    Token() {}
    Token(TokenType type, QString content): _type(type), _content(content) {}

    TokenType type() const { return _type; }
    QString content() const { return _content; }

private:
    TokenType _type;
    QString _content;
};

#endif // TOKEN_H
