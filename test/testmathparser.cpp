#include "testmathparser.h"
#include "mathparser.h"

void TestMathParser::testEmptyFail() {
    QList<Token> tokens;
    QString error;
    MathParser parser;
    parser.parse(tokens, error);

    QVERIFY(!error.isEmpty());
}

void TestMathParser::testInt() {
    QList<Token> tokens;
    tokens << Token(Token::IntegerToken, "123");
    QString error;
    MathParser parser;
    int value = parser.parse(tokens, error);

    QVERIFY(error.isEmpty());
    QCOMPARE(value, 123);
}

void TestMathParser::testInvalidOperator() {
    QList<Token> tokens;
    tokens << Token(Token::OperatorToken, "+");
    QString error;
    MathParser parser;
    parser.parse(tokens, error);

    QVERIFY(!error.isEmpty());
}

void TestMathParser::testAllOperators() {
    QList<Token> tokens;
    tokens << Token(Token::IntegerToken, "1") << Token(Token::OperatorToken, "+") <<
              Token(Token::IntegerToken, "2") << Token(Token::OperatorToken, "-") <<
              Token(Token::IntegerToken, "4") << Token(Token::OperatorToken, "*") <<
              Token(Token::IntegerToken, "3") << Token(Token::OperatorToken, "/") <<
              Token(Token::IntegerToken, "5");
    QString error;
    MathParser parser;
    int value = parser.parse(tokens, error);

    QVERIFY(error.isEmpty());
    QCOMPARE(value, 1);
}

void TestMathParser::testDivisionByZeroFail() {
    QList<Token> tokens;
    tokens << Token(Token::IntegerToken, "1") << Token(Token::OperatorToken, "/") << Token(Token::IntegerToken, "0");
    QString error;
    MathParser parser;
    parser.parse(tokens, error);

    QVERIFY(!error.isEmpty());
}

void TestMathParser::testUnexpectedEndFail() {
    QList<Token> tokens;
    tokens << Token(Token::IntegerToken, "1") << Token(Token::OperatorToken, "+");
    QString error;
    MathParser parser;
    parser.parse(tokens, error);

    QVERIFY(!error.isEmpty());
}
