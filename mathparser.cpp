#include "mathparser.h"

int MathParser::parse(QList<Token> tokens, QString &error) const {
    int value = parseAddSub(tokens, error);
    if (error.isEmpty() && !tokens.isEmpty()) {
        error = QString("Parse error: unexpected token '%1'.").arg(tokens.first().content());
        return 0;
    }

    return value;
}

int MathParser::parseAddSub(QList<Token> &tokens, QString &error) const {
    if (tokens.empty()) {
        error = QString("Parse error: unexpected end of expression.");
        return 0;
    }

    int lhs = parseMulDiv(tokens, error);
    if (!error.isEmpty()) {
        return 0;
    }

    while (!tokens.isEmpty()) {
        Token token = tokens.first();
        QString op = token.content();
        if (token.type() != Token::OperatorToken || (op != "+" && op != "-")) {
            return lhs;
        }

        tokens.removeFirst();

        int rhs = parseMulDiv(tokens, error);
        if (!error.isEmpty()) {
            return 0;
        }

        if (op == "+") {
            lhs += rhs;
        } else if (op == "-") {
            lhs -= rhs;
        }
    }

    return lhs;
}

int MathParser::parseMulDiv(QList<Token> &tokens, QString &error) const {
    if (tokens.empty()) {
        error = QString("Parse error: unexpected end of expression.");
        return 0;
    }

    int lhs = parseInt(tokens, error);
    if (!error.isEmpty()) {
        return 0;
    }

    while (!tokens.isEmpty()) {
        Token token = tokens.first();
        QString op = token.content();
        if (token.type() != Token::OperatorToken || (op != "*" && op != "/")) {
            return lhs;
        }

        tokens.removeFirst();

        int rhs = parseInt(tokens, error);
        if (!error.isEmpty()) {
            return 0;
        }

        if (op == "*") {
            lhs *= rhs;
        } else if (op == "/") {
            if (rhs == 0) {
                error = QString("Parse error: division by zero.");
                return 0;
            }

            lhs /= rhs;
        }
    }

    return lhs;
}

int MathParser::parseInt(QList<Token> &tokens, QString &error) const {
    if (tokens.empty()) {
        error = QString("Parse error: unexpected end of expression.");
        return 0;
    }

    Token token = tokens.takeFirst();
    if (token.type() != Token::IntegerToken) {
        error = QString("Parse error: expected integer, got '%1'.").arg(token.content());
        return 0;
    }

    return token.content().toInt();
}
