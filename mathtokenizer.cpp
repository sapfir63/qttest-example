#include "mathtokenizer.h"

static const QString operators("+-*/");

QList<Token> MathTokenizer::tokenize(QString input, QString &error) const {
    QList<Token> tokens;
    QString currentInteger;

    foreach (QChar c, input) {
        if (c >= '0' && c <= '9') {
            if (currentInteger == "0") {
                error = QString("Tokenizing error: prefixing numbers with zeros not allowed.");
                return QList<Token>();
            }

            currentInteger.append(c);
        } else if (operators.contains(c)) {
            if (!currentInteger.isEmpty()) {
                tokens.append(Token(Token::IntegerToken, currentInteger));
                currentInteger.clear();
            }

            tokens.append(Token(Token::OperatorToken, c));
        } else {
            error = QString("Tokenizing error: unexpected character '%1'.").arg(c);
            return QList<Token>();
        }
    }

    if (!currentInteger.isEmpty()) {
        tokens.append(Token(Token::IntegerToken, currentInteger));
    }

    return tokens;
}
