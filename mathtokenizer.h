#ifndef MATHTOKENIZER_H
#define MATHTOKENIZER_H

#include <QtCore>
#include "token.h"

// Tokenizer of string consisting of numbers and +-*/ operators into number and operator tokens
class MathTokenizer {
public:
    // Splits input string into tokens, sets error in case of tokenizing error
    QList<Token> tokenize(QString input, QString &error) const;
};

#endif // MATHTOKENIZER_H
