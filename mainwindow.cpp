#include <QMessageBox>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mathtokenizer.h"
#include "mathparser.h"

MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);

    // connecting enter and clicking "=" button to computing the result
    connect(ui->computeButton, SIGNAL(toggled(bool)), this, SLOT(computeResult()));
    connect(ui->expressionInput, SIGNAL(returnPressed()), this, SLOT(computeResult()));
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::computeResult() {    
    MathTokenizer tokenizer;
    QString error;
    // tokenizing input string and informing user about error if it happened
    QList<Token> tokens = tokenizer.tokenize(ui->expressionInput->text(), error);
    if (!error.isEmpty()) {
        QMessageBox::critical(this, "Error", error);
        ui->resultOutput->setText("ERR");
        return;
    }

    MathParser parser;
    // parsing sequence of tokens and informing user about error if it happened
    int value = parser.parse(tokens, error);
    if (!error.isEmpty()) {
        QMessageBox::critical(this, "Error", error);
        ui->resultOutput->setText("ERR");
        return;
    }

    // writing the result into output field
    ui->resultOutput->setText(QString::number(value));
}
